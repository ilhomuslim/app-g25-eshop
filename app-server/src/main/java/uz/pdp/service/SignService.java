package uz.pdp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.entity.RoleName;
import uz.pdp.entity.UserStatus;
import uz.pdp.entity.Users;
import uz.pdp.model.Result;
import uz.pdp.payload.ReqSignIn;
import uz.pdp.payload.ReqSignUp;
import uz.pdp.repository.RoleRepository;
import uz.pdp.repository.StatusRepository;
import uz.pdp.repository.UserRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class SignService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    StatusRepository statusRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    JavaMailSender sender;

    public Result getSignUp(ReqSignUp reqSignUp){
        Result result = new Result();
        Optional<Users> byEmail = userRepository.findByEmail(reqSignUp.getEmail());
        Optional<Users> byUsername = userRepository.findByUsername(reqSignUp.getUserName());
        if (byEmail.isPresent()){
            result.setStatus(false);
            result.setMessage(reqSignUp.getEmail() + " is exist in the system!");
        }else if (byUsername.isPresent()){
            result.setStatus(false);
            result.setMessage(reqSignUp.getUserName() + " is exist in the system!");
        }else {
            Users newUser = new Users(
                    null,
                    reqSignUp.getUserName(),
                    passwordEncoder.encode(reqSignUp.getPassword()),
                    null,
                    reqSignUp.getEmail(),
                    statusRepository.findByType(UserStatus.REGISTERED),
                    roleRepository.findAllByRoleName(RoleName.ROLE_USER),
                    0.0,
                    UUID.randomUUID()
            );

            userRepository.save(newUser);

            SimpleMailMessage messages = new SimpleMailMessage();
            messages.setSubject("Eshop user verification");
            messages.setTo(newUser.getEmail());
            messages.setText("<h3>Welcome to Eshop " + newUser.getUsername() + "!</h3><br>" +
                    "<br> http://localhost/api/activation?token=" + newUser.getToken() + "&email=" + newUser.getEmail());

            /*sender.send(messages);*/

            result.setStatus(true);
            result.setMessage("Success!");
        }
        return result;
    }

    public Result getSignIn(ReqSignIn reqSignIn){
        Result result = new Result();
        Optional<Users> byUsername = userRepository.findByUsername(reqSignIn.getUserName());
        if (byUsername.isPresent()){
            boolean matches = passwordEncoder.matches(reqSignIn.getPassword(), byUsername.get().getPassword());
            if (matches){
                result.setStatus(true);
                result.setMessage("Signed in");
            }
            else {
                result.setStatus(false);
                result.setMessage("Username or password wrong!");
            }
        }else {
            result.setStatus(false);
            result.setMessage("Username or password wrong!");
        }

        return result;
    }
}
