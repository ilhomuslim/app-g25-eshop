package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.Company;
import uz.pdp.entity.Order;
import uz.pdp.projection.OrderProjection;

import java.util.UUID;

@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "order", excerptProjection = OrderProjection.class)
public interface OrderRepository extends JpaRepository<Order, UUID> {
}
