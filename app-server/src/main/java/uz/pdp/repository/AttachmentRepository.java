package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.Attachment;
import uz.pdp.projection.AttachmentProjection;

import java.util.UUID;
@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "attachment", excerptProjection = AttachmentProjection.class)
public interface AttachmentRepository extends JpaRepository <Attachment, UUID>{

}
