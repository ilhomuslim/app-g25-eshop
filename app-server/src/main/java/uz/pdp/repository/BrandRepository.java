package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.Brand;
import uz.pdp.projection.BrandProjection;

import java.util.UUID;

@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "brand", excerptProjection = BrandProjection.class)
public interface BrandRepository extends JpaRepository<Brand, UUID> {
}
