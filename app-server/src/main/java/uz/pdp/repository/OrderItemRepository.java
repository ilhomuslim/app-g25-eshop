package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.Order;
import uz.pdp.entity.OrderItem;
import uz.pdp.projection.OrderItemProjection;

import java.util.UUID;

@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "order_item", excerptProjection = OrderItemProjection.class)
public interface OrderItemRepository extends JpaRepository<OrderItem, UUID> {
}
