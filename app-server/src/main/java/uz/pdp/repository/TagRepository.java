package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.Status;
import uz.pdp.entity.Tag;
import uz.pdp.projection.TagProjection;

import java.util.UUID;
@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "tag", excerptProjection = TagProjection.class)
public interface TagRepository extends JpaRepository<Tag, UUID> {
}
