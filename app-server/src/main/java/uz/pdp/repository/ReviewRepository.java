package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.Rating;
import uz.pdp.entity.Review;
import uz.pdp.projection.ReviewProjection;

import java.util.UUID;

@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "reviews", excerptProjection = ReviewProjection.class)
public interface ReviewRepository extends JpaRepository<Review, UUID> {
}
