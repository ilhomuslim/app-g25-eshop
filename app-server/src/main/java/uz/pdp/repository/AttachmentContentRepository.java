package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.AttachmentContent;
import uz.pdp.projection.AttachmentContentProjection;

import java.util.UUID;

@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "attachmentContent", excerptProjection = AttachmentContentProjection.class)
public interface AttachmentContentRepository extends JpaRepository <AttachmentContent, UUID>{
}
