package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.Category;
import uz.pdp.entity.Company;
import uz.pdp.projection.CompanyProjection;

import java.util.UUID;

@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "company", excerptProjection = CompanyProjection.class)
public interface CompanyRepository extends JpaRepository<Company, UUID> {
}
