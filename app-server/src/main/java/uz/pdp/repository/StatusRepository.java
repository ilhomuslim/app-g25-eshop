package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.Role;
import uz.pdp.entity.Status;
import uz.pdp.entity.UserStatus;
import uz.pdp.projection.StatusProjection;

@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "users_status", excerptProjection = StatusProjection.class)
public interface StatusRepository extends JpaRepository<Status, Integer> {
    Status findByType(UserStatus status);
}
