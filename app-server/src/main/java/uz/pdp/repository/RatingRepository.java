package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.Product;
import uz.pdp.entity.Rating;
import uz.pdp.projection.RatingProjection;

import java.util.UUID;

@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "rating", excerptProjection = RatingProjection.class)
public interface RatingRepository extends JpaRepository<Rating, UUID> {
}
