package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import uz.pdp.entity.Review;
import uz.pdp.entity.Role;
import uz.pdp.entity.RoleName;
import uz.pdp.projection.RoleProjection;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = {"http://localhost:3000"})
@RepositoryRestResource(path = "user_role", excerptProjection = RoleProjection.class)
public interface RoleRepository extends JpaRepository<Role, Integer> {
    List<Role> findAllByRoleName(RoleName roleName);
}
