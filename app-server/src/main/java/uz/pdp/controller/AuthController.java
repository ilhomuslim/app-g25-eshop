package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.model.JwtAuthenticationResponse;
import uz.pdp.model.Result;
import uz.pdp.payload.ReqSignIn;
import uz.pdp.payload.ReqSignUp;
import uz.pdp.security.JwtTokenProvider;
import uz.pdp.service.SignService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
public class AuthController {

    @Autowired
    SignService signService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @PostMapping("/api/sign/up")
    public HttpEntity<?> getSignUp(@Valid @RequestBody ReqSignUp reqSignUp){
        Result result = signService.getSignUp(reqSignUp);
        if (result.isStatus()){
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(reqSignUp.getUserName(), reqSignUp.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = jwtTokenProvider.generateToken(authentication);

            return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
        }
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/api/sign/in")
    public HttpEntity<?> getSignIn(@Valid @RequestBody ReqSignIn reqSignIn){
       /* Result result = signService.getSignIn(reqSignIn);
        if (result.isStatus()){

        }else {
            return ResponseEntity.ok().body(result);
        }*/

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(reqSignIn.getUserName(), reqSignIn.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtTokenProvider.generateToken(authentication);

        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }



    @GetMapping("/api/activation")
    public HttpEntity<?> getActiveUser(@RequestParam UUID token, @RequestParam String email){
        //http://localhost/api/activation/?token=90fdsaf-fdsa-fdsfra&email=154531564651321564
        System.out.println(token + " || " + email);
        return ResponseEntity.ok().body(new Result());
    }
}
