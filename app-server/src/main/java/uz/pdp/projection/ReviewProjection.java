package uz.pdp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Review;
import uz.pdp.entity.Users;

import java.util.UUID;

@Projection(types = Review.class, name = "reviewProjection")
public interface ReviewProjection {
    UUID getId();

    String getText();

    @Value("#{target.users.fullName}")
    String getUserFullName();

    String getCreatedAt();
}
