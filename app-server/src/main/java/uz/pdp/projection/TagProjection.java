package uz.pdp.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Tag;

import java.util.UUID;

@Projection(types = Tag.class, name = "tagProjection")
public interface TagProjection {
    UUID getId();
    String getName();
}
