package uz.pdp.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Category;
import uz.pdp.entity.Company;

import java.util.UUID;

@Projection(types = Category.class, name = "categoryProjection")
public interface CategoryProjection {
    UUID getId();
    String getName();
}
