package uz.pdp.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Brand;
import uz.pdp.entity.Category;

import java.util.List;
import java.util.UUID;

@Projection(types = Brand.class, name = "brandProjection")
public interface BrandProjection {
    UUID getId();
    String getName();
    List<Category> getCategories();
}
