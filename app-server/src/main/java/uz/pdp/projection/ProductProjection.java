package uz.pdp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Attachment;
import uz.pdp.entity.Product;
import uz.pdp.entity.Rating;
import uz.pdp.entity.Tag;

import java.util.List;
import java.util.UUID;

@Projection(types = Product.class, name = "productProjection")
public interface ProductProjection {
    UUID getId();

    String getName();

    List<Attachment> getAttachments();

    Integer getWebId();

    Double getPrice();

    Integer getCount();

    String getCondition();

    @Value("#{target.category.name}")
    String getCategoryName();

    @Value("#{target.brand.name}")
    String getBrandName();

    String getDetails();

    List<Tag> getTags();

    @Value("#{target.company.id}")
    UUID getCompanyId();

    @Value("#{target.company.name}")
    String getCompanyName();
}
