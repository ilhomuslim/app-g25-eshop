package uz.pdp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Order;
import uz.pdp.entity.OrderItem;

import java.util.List;
import java.util.UUID;

@Projection(types = Order.class, name = "orderProjection")
public interface OrderProjection {
    UUID getId();

    @Value("#{target.users.id}")
    String getUserId();

    @Value("#{target.users.fullname}")
    String getUserFullName();

    List<OrderItem> getOrderItems();
}
