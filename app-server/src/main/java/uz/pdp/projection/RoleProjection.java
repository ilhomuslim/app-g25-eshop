package uz.pdp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Role;
import uz.pdp.entity.Status;

@Projection(types = Role.class, name = "roleProjection")
public interface RoleProjection {
    Integer getId();
    @Value("#{target.roleName.name()}")
    String getUserRoleName();
}
