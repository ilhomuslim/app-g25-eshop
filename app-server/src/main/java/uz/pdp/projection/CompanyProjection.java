package uz.pdp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Attachment;
import uz.pdp.entity.Company;
import uz.pdp.entity.Product;
import uz.pdp.entity.Tag;

import java.util.List;
import java.util.UUID;

@Projection(types = Company.class, name = "companyProjection")
public interface CompanyProjection {
    UUID getId();
    String getName();
    String getPhoneNumber();
    String getWebSite();
    String getAddress();
}
