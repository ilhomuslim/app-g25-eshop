package uz.pdp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Role;
import uz.pdp.entity.Users;

import java.util.List;
import java.util.UUID;

@Projection(types = Users.class, name = "userProjection")
public interface UserProjection {
    UUID getId();

    String getFullName();

    String getUsername();

    String getPhoneNumber();

    String getEmail();

    Boolean getDeleted();

    @Value("#{target.status.type}")
    String getStatusType();

    @Value("#{target.role}")
    List<Role> getRoleName();

    Double getBalance();
}
