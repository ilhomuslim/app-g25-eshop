package uz.pdp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.OrderItem;
import uz.pdp.entity.Rating;

import java.util.UUID;

@Projection(types = OrderItem.class, name = "orderItemProjection")
public interface OrderItemProjection {
    UUID getId();

    @Value("#{target.product.id}")
    String getProductId();

    @Value("#{target.product.name}")
    String getProductName();

    Integer getQuantity();
}
