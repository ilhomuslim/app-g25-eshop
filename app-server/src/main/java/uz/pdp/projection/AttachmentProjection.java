package uz.pdp.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Attachment;
import uz.pdp.entity.Brand;
import uz.pdp.entity.Category;

import java.util.List;
import java.util.UUID;

@Projection(types = Attachment.class, name = "attachmentProjection")
public interface AttachmentProjection {
    UUID getId();

    String getName();

    Long getSize();

    String getType();
}
