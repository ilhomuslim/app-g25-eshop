package uz.pdp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Attachment;
import uz.pdp.entity.AttachmentContent;

import java.util.UUID;

@Projection(types = AttachmentContent.class, name = "attachmentContentProjection")
public interface AttachmentContentProjection {
    UUID getId();

    String getContent();

    @Value("#{target.attachment.id}")
    UUID getAttachmentId();
}
