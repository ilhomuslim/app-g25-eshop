package uz.pdp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Rating;
import uz.pdp.entity.Users;

import java.util.UUID;

@Projection(types = Rating.class, name = "ratingProjection")
public interface RatingProjection {
    UUID getId();

    @Value("#{target.users.fullName}")
    String getUserFullName();

    Integer getScore();
}
