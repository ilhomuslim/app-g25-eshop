package uz.pdp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.entity.Status;
import uz.pdp.entity.Tag;

import java.util.UUID;

@Projection(types = Status.class, name = "statusProjection")
public interface StatusProjection {
    Integer getId();
    @Value("#{target.type.name()}")
    String getUserStatusName();
}
