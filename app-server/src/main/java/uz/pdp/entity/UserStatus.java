package uz.pdp.entity;

public enum UserStatus {
    REGISTERED,
    ACTIVE,
    BLOCKED
}
