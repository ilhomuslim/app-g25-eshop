insert into role(id, role_name) values (10, 'ROLE_USER');
insert into role(id, role_name) values (20, 'ROLE_ADMIN');
insert into role(id, role_name) values (30, 'ROLE_MODERATOR');

insert into status(id, type) values (10, 'REGISTERED');
insert into status(id, type) values (20, 'ACTIVE');
insert into status(id, type) values (30, 'BLOCKED');
