import React, {Component} from 'react';
import {Button, Col, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row} from "reactstrap";

class ProfileDetailInfo extends Component {
    render() {
        return (
            <div>
                <Row>
                    <Col>
                        <label className="pt-2 text-dark">Username: </label>
                    </Col>
                    <Col>
                        <label className="pt-2 text-dark font-weight-bolder">E-Shopper</label>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <label className="pt-2 text-dark">E-mail: </label>
                    </Col>
                    <Col>
                        <label className="pt-2 text-dark font-weight-bolder">info@www.uz</label>
                    </Col>
                </Row>
                <Button className="mt-2 btn-link text-dark" href="/profile_edit" color="light">
                    Edit
                </Button>
            </div>
        );
    }
}

export default ProfileDetailInfo;