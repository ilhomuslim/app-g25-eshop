import React, {Component} from 'react';
import {Button, Col, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row} from "reactstrap";

class ProfileDetailInfo extends Component {
    render() {
        return (
            <div>
                <InputGroup className="form-group-no-border my-2">
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                            Username:
                        </InputGroupText>
                    </InputGroupAddon>
                    <Input id="signInUsername" placeholder="Username" type="text"/>
                </InputGroup>
                <InputGroup className="form-group-no-border my-2">
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                            E-mail:
                        </InputGroupText>
                    </InputGroupAddon>
                    <Input id="signInUsername" placeholder="E-mail" type="text"/>
                </InputGroup>
                <InputGroup className="form-group-no-border my-2">
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                            Password:
                        </InputGroupText>
                    </InputGroupAddon>
                    <Input id="signInUsername" placeholder="Password" type="text"/>
                </InputGroup>
                <Button className="mt-2 btn-link text-light" href="/profile" color="info">
                    Save
                </Button>
            </div>
        );
    }
}

export default ProfileDetailInfo;