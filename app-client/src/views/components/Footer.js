import React from "react";

// Components - reactstrap
import {Row, Container} from "reactstrap";

function Footer() {
    return (
        <footer className="fixed-bottom footer footer-black footer-white bg-light">
            <Container>
                <Row>
                    <nav className="footer-nav">
                        <ul>
                            <a className="text-dark" href="#" target="_blank">
                                SERVICE
                            </a>
                            -
                            <a className="text-dark" href="#" target="_blank">
                                QUOCK SHOP
                            </a>
                            -
                            <a className="text-dark" href="#" target="_blank">
                                POLICIES
                            </a>
                            -
                            <a className="text-dark" href="#" target="_blank">
                                POLICIES
                            </a>
                        </ul>
                    </nav>
                    <div className="credits ml-auto">
                        <span className="copyright">
                          © {new Date().getFullYear()}, made with{" "}
                            <i className="fa fa-heart heart"/>{" "}by <b>www.uz</b>
                        </span>
                    </div>
                </Row>
            </Container>
        </footer>
    );
}

export default Footer;
