import React, {Component} from 'react';

// Components - reactstrap
import {Card, CardImg, CardBody, CardTitle, CardSubtitle, CardLink, Button} from "reactstrap";

class ProductCards extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Card>
                {/*<CardImg src={require("/assets/img/faces/erik-lucatero-2.jpg")} alt="Card image cap"/>*/}
                <CardBody>
                    <CardTitle className="p-2">{this.props.products.price}</CardTitle>
                    <CardSubtitle className="p-2">{this.props.products.name}</CardSubtitle>
                    <CardLink href="/product_details" className="p-2">Product Info</CardLink>
                    <Button className="btn-round ml-1 p-2" color="info" type="button">
                        <i className="mr-2 nc-icon nc-cart-simple"/>
                        Add to Cart
                    </Button>
                </CardBody>
            </Card>
        );
    }
}

export default ProductCards;