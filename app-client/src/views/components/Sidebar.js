import React, {useState} from "react";

// reactstrap components
import {Collapse, Navbar, NavbarToggler, NavbarBrand, NavItem, NavLink, Nav,} from "reactstrap";

function SectionProgress() {
    const [collapsed, setCollapsed] = useState(true)
    const toggleNavbar = () => setCollapsed(!collapsed)

    const [collapsedBrands, setCollapsedBrands] = useState(true)
    const toggleNavbarBrands = () => setCollapsedBrands(!collapsedBrands)

    return (
        <div>
            <h3 className="mr-2 text-center">Category</h3>
            <Navbar color="faded" light>
                <NavbarBrand className="mr-auto">Category</NavbarBrand>
                <NavbarToggler onClick={toggleNavbar} className="mr-2"/>
                <Collapse isOpen={!collapsed} navbar>
                    <Nav navbar>
                        <NavItem>
                            <NavLink href="/product" className="text-dark">Sub Category 1</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/product" className="text-dark">Sub Category 2</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/product" className="text-dark">Sub Category 3</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/product" className="text-dark">Sub Category 4</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
            <Navbar color="faded" light>
                <NavbarBrand className="mr-auto">Category</NavbarBrand>
            </Navbar>
            <h3 className="mr-2 text-center">Brands</h3>
            <Navbar color="faded" light>
                <NavbarBrand className="mr-auto">Category</NavbarBrand>
                <NavbarToggler onClick={toggleNavbarBrands} className="mr-2"/>
                <Collapse isOpen={!collapsedBrands} navbar>
                    <Nav navbar>
                        <NavItem>
                            <NavLink href="/product" className="text-dark">Sub Category 1</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/product" className="text-dark">Sub Category 2</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/product" className="text-dark">Sub Category 3</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/product" className="text-dark">Sub Category 4</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
            <Navbar color="faded" light>
                <NavbarBrand className="mr-auto">Category</NavbarBrand>
            </Navbar>
        </div>
    );
}

export default SectionProgress;
