import React from "react";

// nodejs library that concatenates strings
import classnames from "classnames";

// reactstrap components
import {
    Collapse,
    Navbar,
    NavItem,
    NavLink,
    Nav,
    Container,
    UncontrolledCollapse
} from "reactstrap";

function IndexNavbar() {
    return (
        <Navbar className="fixed-top bg-info" expand="lg">
            <Container>
                <UncontrolledCollapse navbar toggler="#navbar-menu">
                    <Nav className="ml-auto" navbar>
                        <NavItem className="active">
                            <NavLink href="#" onClick={e => e.preventDefault()}>
                                <p className="ml-2 text-light">+998 99 437 0770</p>
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#" onClick={e => e.preventDefault()}>
                                <p className="ml-2 text-light">info@www.uz</p>
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <Collapse className="justify-content-end" navbar>
                        <Nav navbar>
                            <NavItem>
                                <NavLink data-placement="bottom" href="#" target="_blank" title="Follow us on Twitter">
                                    <p className="d-lg text-light">Twitter</p>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink data-placement="bottom" href="#" target="_blank" title="Like us on Facebook">
                                    <p className="d-lg text-light">Facebook</p>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink data-placement="bottom" href="#" target="_blank"
                                         title="Follow us on Instagram">
                                    <p className="d-lg text-light">Instagram</p>
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </UncontrolledCollapse>
            </Container>
        </Navbar>
    );
}

export default IndexNavbar;
