import React from "react";

// reactstrap components
import {
    UncontrolledCollapse,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    NavbarBrand,
    Navbar,
    NavItem,
    NavLink,
    Nav,
    Container,
    Row,
    Col
} from "reactstrap";

function NavigationBar() {
    return (
        <div className="mt-5 pt-lg-2 bg-dark">
            <Container id="menu-dropdown">
                <Row>
                    <Col md="12">
                        <Navbar expand="lg">
                            <NavbarBrand className="text-light" href="/">
                                Logo
                            </NavbarBrand>
                            <button
                                aria-controls="navbarSupportedContent"
                                aria-expanded={false}
                                aria-label="Toggle navigation"
                                className="navbar-toggler navbar-toggler-right"
                                data-target="#navbar-menu"
                                data-toggle="collapse"
                                id="navbar-menu"
                                type="button">
                                <span className="navbar-toggler-bar"/>
                                <span className="navbar-toggler-bar"/>
                                <span className="navbar-toggler-bar"/>
                            </button>
                            <UncontrolledCollapse navbar toggler="#navbar-menu">
                                <Nav className="mr-auto" navbar>
                                    <UncontrolledDropdown nav inNavbar>
                                        <DropdownToggle
                                            aria-expanded={false} aria-haspopup={true} caret
                                            color="default" data-toggle="dropdown" href="#pablo"
                                            id="dropdownMenuButton" nav role="button" className="text-light">
                                            USA
                                        </DropdownToggle>
                                        <DropdownMenu aria-labelledby="dropdownMenuButton" className="dropdown-info">
                                            <DropdownItem header tag="span">USA</DropdownItem>
                                            <DropdownItem>Canada</DropdownItem>
                                            <DropdownItem>UK</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                    <UncontrolledDropdown nav inNavbar>
                                        <DropdownToggle
                                            aria-expanded={false} aria-haspopup={true} caret color="default"
                                            data-toggle="dropdown" href="#pablo" id="dropdownMenuButton" nav
                                            role="button" className="text-light">
                                            Dollar
                                        </DropdownToggle>
                                        <DropdownMenu aria-labelledby="dropdownMenuButton"
                                                      className="dropdown-info text-light">
                                            <DropdownItem>Canadian Dollar</DropdownItem>
                                            <DropdownItem>Pound</DropdownItem>
                                            <DropdownItem header tag="span">Dollar</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </Nav>
                                <Nav className="ml-auto" navbar>
                                    <NavItem className="active">
                                        <NavLink href="/e_shopper">
                                            <p className="ml-2 text-light">E-shopper</p>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem className="active">
                                        <NavLink href="/dashboard">
                                            <p className="ml-2 text-light">Dashboard</p>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem className="active">
                                        <NavLink href="/cart">
                                            <p className="ml-2 text-light">Cart</p>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/login">
                                            <p className="ml-2 text-light">Login</p>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/profile">
                                            <p className="ml-2 text-light">Profile</p>
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                            </UncontrolledCollapse>
                        </Navbar>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default NavigationBar;
