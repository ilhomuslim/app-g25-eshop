import React, {Component} from 'react';

// Components - reactstrap
import {Col, Container, Row,} from "reactstrap";
import {Card, CardImg, CardBody, CardTitle, CardSubtitle, CardLink, Button} from "reactstrap";

// Components
import CardItem from "./CardItem";
import axios from "axios";
import {connect} from 'react-redux'

class ProductCards extends Component {

    componentDidMount() {
        this.refreshList();
    }

    refreshList = () => {
        axios.get("http://localhost/api/product",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
               /* console.log(data.data._embedded.products);*/
                this.props.setProductsList(data.data._embedded.products);
            });
    };

    render() {
        let productCards = this.props.productList.map(product => {
            return (
                <Col sm="4">
                    <Card>
                        {/*<CardImg src={require("/assets/img/faces/erik-lucatero-2.jpg")} alt="Card image cap"/>*/}
                        <CardBody>
                            <CardTitle className="p-2">{product.price}</CardTitle>
                            <CardSubtitle className="p-2">{product.name}</CardSubtitle>
                            <CardLink href="/product_details" className="p-2">Product Info</CardLink>
                            <Button className="btn-round ml-1 p-2" color="info" type="button">
                                <i className="mr-2 nc-icon nc-cart-simple"/>
                                Add to Cart
                            </Button>
                        </CardBody>
                    </Card>
                </Col>
            )
        });
        return (
            <Container fluid>
                <Row className="p-2">
                    {productCards}
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    setProductsList: products => dispatch({type:"productList", productList:products}),
});

export default connect (mapStateToProps, mapDispatchToProps) (ProductCards);