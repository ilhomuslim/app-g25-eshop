import React from "react";

// reactstrap components
import {
    UncontrolledCollapse,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    NavbarBrand,
    Navbar,
    NavItem,
    NavLink,
    Nav,
    Container,
    Row,
    Col
} from "reactstrap";

function NavigationBarDashboard() {
    return (
        <div className="bg-dark">
            <Container id="menu-dropdown">
                <Row>
                    <Col md="12">
                        <Navbar expand="lg">
                            <NavbarBrand className="text-light" href="/dashboard">
                                Logo
                            </NavbarBrand>
                            <button
                                aria-controls="navbarSupportedContent"
                                aria-expanded={false}
                                aria-label="Toggle navigation"
                                className="navbar-toggler navbar-toggler-right"
                                data-target="#navbar-menu"
                                data-toggle="collapse"
                                id="navbar-menu"
                                type="button">
                                <span className="navbar-toggler-bar"/>
                                <span className="navbar-toggler-bar"/>
                                <span className="navbar-toggler-bar"/>
                            </button>
                            <UncontrolledCollapse navbar toggler="#navbar-menu">
                                <Nav className="ml-auto" navbar>
                                    <NavItem className="active">
                                        <NavLink href="/e_shopper">
                                            <p className="ml-2 text-light">E-shopper</p>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/dashboard_login">
                                            <p className="ml-2 text-light">Login</p>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/dashboard_profile">
                                            <p className="ml-2 text-light">Profile</p>
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                            </UncontrolledCollapse>
                        </Navbar>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default NavigationBarDashboard;
