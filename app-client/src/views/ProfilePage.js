import React from "react";

import {Col, Container, Media, Row} from "reactstrap";

// Components
import Footer from "./components/Footer";
import NavigationBar from "./components/NavigationBar";
import Header from "./components/Header";

//Pages
import ProfileDetailInfo from "./profile/ProfileDetailInfo";

function Shop() {
    return (
        <>
            {/*Headers*/}
            <Header/>
            <NavigationBar/>

            {/*Profile Form*/}
            <Container>
                <Row>
                    <Col md="3">
                        <Media list>
                            <Media tag="li">
                                <Media body>
                                    <Media heading>
                                        User Picture
                                    </Media>
                                </Media>
                            </Media>
                            <Media tag="li">
                                <Media left href="#">
                                    <Media object data-src="holder.js/64x64"/>
                                </Media>
                            </Media>
                        </Media>
                    </Col>
                    <Col md="9">
                        <ProfileDetailInfo/>
                    </Col>
                </Row>
            </Container>

            {/*Footers*/}
            <Footer/>
        </>
    );
}

export default Shop;
