import React, {Component} from 'react';

// Components
import Header from "./components/Header.js";
import Footer from "./components/Footer.js";

// Pages
import NavigationBar from "./components/NavigationBar";
import CartList from "./cart/CartList";

class CartPage extends Component {
    render() {
        return (
            <>
                {/*Headers*/}
                <Header/>
                <NavigationBar/>

                {/*Cart List*/}
                <CartList/>

                {/*Footers*/}
                <Footer/>
            </>
        );
    }
}

export default CartPage;