import React, {useState} from "react";

// reactstrap components
import {Collapse, Navbar, NavbarToggler, NavbarBrand, NavItem, NavLink, Nav,} from "reactstrap";

function SectionProgress() {
    const [collapsed, setCollapsed] = useState(true)
    const toggleNavbar = () => setCollapsed(!collapsed)

    const [collapsedBrands, setCollapsedBrands] = useState(true)
    const toggleNavbarBrands = () => setCollapsedBrands(!collapsedBrands)

    return (
        <div>
            <h3 className="mr-2 text-center">Menu</h3>
            <Nav navbar>
                <NavItem>
                    <NavLink href="/dashboard_client" className="text-dark">Clients</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/dashboard_category" className="text-dark">Categories</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/dashboard_brand" className="text-dark">Brands</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/dashboard_product" className="text-dark">Products</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/dashboard_company" className="text-dark">Companies</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="/dashboard_tag" className="text-dark">Tags</NavLink>
                </NavItem>
            </Nav>
        </div>
    );
}

export default SectionProgress;
