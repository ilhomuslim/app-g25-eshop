import React, {Component} from 'react';
import {
    Container, Row, Col, Table, Button,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form, FormGroup, Label, Input, InputGroup, InputGroupAddon, InputGroupText
} from 'reactstrap'
import axios from 'axios';
import {connect} from "react-redux";

class Tag extends Component {
    state={
        isModalShow:false,
        isModalShow1:false,
        isModalShow2:false,
        currentClient:{}
    };
    handleEdit = tag => {
        this.setState({
            currentClient:tag
        });
        this.openAddModal1()
    };

    handleDelete = tag => {
        this.setState({
            currentClient:tag
        });
        this.openAddModal2()
    };


    openAddModal1 = () => {
        this.setState({
            isModalShow1:!this.state.isModalShow1
        })
    };
    openAddModal2 = () => {
        this.setState({
            isModalShow2:!this.state.isModalShow2
        })
    };


    componentDidMount() {
        this.refreshList();
    }

    refreshList = () => {
        axios.get("http://localhost/api/tag",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then( list => {
                console.log(list);
                this.props.setTagList(list.data._embedded.tags);
            })
    };

    editTag = () => {
        const name = document.getElementById("companyName").value;
        axios.put("http://localhost/api/tag/"+this.state.currentClient.id,
            {"name": name},
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                this.refreshList();
                this.openAddModal1();
            });
    };
    deleteTag = () =>{
        axios.delete("http://localhost/api/tag/"+this.state.currentClient.id,
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                this.refreshList();
                this.openAddModal2();
            });

        // console.log(this.state.currentClient.id);
    };

    render() {
        return (
            <Container>
                <Row>
                    <Col md="12">
                        <Button color="secondary" onClick={this.openModal}>Add tag</Button>
                        <Table className="table-striped">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Operation</th>
                            </tr>
                            </thead>
                            <tbody>{console.log(this.props.companyTag)}
                            {this.props.companyTag.map(tag =>
                                {
                                    return <tr key={tag.id}>
                                        <td>{tag.id}</td>
                                        <td>{tag.name}</td>
                                        <td>
                                            <Button color="primary" onClick={() => this.handleEdit(tag)}>Edit</Button>{' '}
                                            <Button color="danger" onClick={() => this.handleDelete(tag)}>Delete</Button>
                                        </td>
                                    </tr>
                                }
                            )}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Modal isOpen={this.state.isModalShow} toggle={this.openModal}>
                    <ModalHeader>Add tag</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="name">Name</Label>
                                <Input type="text"
                                       name="name"
                                       id="name"
                                       placeholder="Enter name" />
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.handleSave}>Save</Button>
                        <Button color="secondary" onClick={this.openModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.isModalShow1} toggle={this.openAddModal1}>
                    <ModalHeader toggle={this.openAddModal1}>Edit Tag</ModalHeader>
                    <ModalBody>
                        <Form className="register-form">
                            <label className="pt-2 text-light">Company name</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="companyName" defaultValue={this.state.currentClient.name} placeholder="Company name" type="text"/>
                            </InputGroup>

                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.editTag}>Edit Tag</Button>{' '}
                        <Button color="secondary" onClick={this.openAddModal1}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.isModalShow2} toggle={this.openAddModal2}>
                    <ModalHeader toggle={this.openAddModal2}>Delete Tag</ModalHeader>
                    <ModalBody>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                        dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                        mollit anim id est laborum.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.deleteTag}>Do Something</Button>{' '}
                        <Button color="secondary" onClick={this.openAddModal2}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </Container>
        );
    }
    openModal = () => {
        this.setState({

            isModalShow:!this.state.isModalShow
        });
    };

    handleSave = () => {
        const name = document.getElementById("name").value;
        axios.post("http://localhost/api/tag",
            {"name":name},
            {headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }})
            .then(atag => {
                this.openModal();
                this.refreshList();
            })
            .catch(error => {
                console.log(error)
            })
    };


}
const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    setTagList: tagLast => dispatch({type:"companyTag", companyTag:tagLast})
});

export default connect(mapStateToProps, mapDispatchToProps)(Tag);