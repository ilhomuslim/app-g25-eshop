import React, {Component} from 'react';

import {Col, Container, Row} from "reactstrap";

import Header from "./components/Header";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";

// Pages
import ShopCarousel from "./shop/ShopCarousel.js";
import Content from "./shop/ShopContent.js";
import Sidebar from "./components/Sidebar";

class ShopPage extends Component {
    render() {
        return (
            <>
                {/*Headers*/}
                <Header/>
                <NavigationBar/>

                {/*Slider - Carousel & Sidebar -Category/Brands & Content - Product List*/}
                <ShopCarousel/>
                <Container className="pb-lg-5">
                    <Row>
                        <Col className="p-2" md="3">
                            <Sidebar/>
                        </Col>
                        <Col className="p-2" md="9">
                            <Content/>
                        </Col>
                    </Row>
                </Container>

                {/*Footers*/}
                <Footer/>
            </>
        );
    }
}

export default ShopPage;