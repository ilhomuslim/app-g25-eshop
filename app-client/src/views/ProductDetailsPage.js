import React from "react";

// Components - reactstrap
import {Col, Container, Row} from "reactstrap";

// Components
import Header from "./components/Header.js";

import Footer from "./components/Footer.js";
// Pages
import NavigationBar from "./components/NavigationBar";
import Sidebar from "./components/Sidebar";
import ProductCards from "./components/ProductCards";
import ProductDetailsTab from "./product_details/ProductTab";
import ProductInfo from "./product_details/ProductInfo";

function Shop() {
    return (
        <>
            {/*Headers*/}
            <Header/>
            <NavigationBar/>

            {/*Product Details*/}
            <Container>
                <Row>
                    <Col md="3">
                        <Sidebar/>
                    </Col>
                    <Col md="9">
                        <div>
                            {/*Product Info*/}
                            <h3 className="mr-2 text-center p-2">Product Info</h3>
                            <ProductInfo/>

                            {/*Categories Tabs*/}
                            <ProductDetailsTab/>

                            {/*Recommended*/}
                            <h3 className="mr-2 text-center p-2">Recommended</h3>
                            <ProductCards/>
                        </div>
                    </Col>
                </Row>
            </Container>

            {/*Footers*/}
            <Footer/>
        </>
    );
}

export default Shop;
