import React, {Component} from 'react';
import {Button, Col, Container, Row, Table} from "reactstrap";

class CartListHeader extends Component {
    render() {
        return (
            <div>
                <Row className="name-row bg-secondary p-3 text-light">
                    <Col className="text-center" md={7}> Product</Col>
                    <Col className="text-center" md={1}>Price </Col>
                    <Col className="text-center" md={2}>Quantity</Col>
                    <Col className="text-center" md={1}> Total</Col>
                    <Col className="text-center" md={1}>{" "}</Col>
                </Row>
            </div>
        );
    }
}

export default CartListHeader;