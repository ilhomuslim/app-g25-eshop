import React, {Component} from 'react';
import {Button, Col, Row} from "reactstrap";

class CartList extends Component {
    render() {
        return (
            <div>
                <Row className="border">
                    <Col md={2}>
                        <img className="m-2" width="100" alt=""/>
                    </Col>
                    <Col className="align-self-center" md={5}>
                        <h5>Colorblock Scuba</h5>
                        <p>Web Id:100783948</p>
                    </Col>
                    <Col className="align-self-center" md={1}><p>{this.props.cartList.price}</p></Col>
                    <Col className="button align-self-md-center" md={2}>
                        <Button className="mx-1">+</Button>
                        <Button className="mx-1">{this.props.cartList.count}</Button>
                        <Button className="mx-1">-</Button>
                    </Col>
                    <Col className="align-self-center" md={1}>
                        <p className="d-inline-block">69 $</p>
                    </Col>
                    <Col className="align-self-center" md={1}>
                        <Button className="d-inline-block ml-3"> x</Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default CartList;