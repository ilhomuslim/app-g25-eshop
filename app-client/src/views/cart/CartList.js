import React, {Component} from 'react';

import {Col, Container, Row} from "reactstrap";

import CartItem from "./CartIListItem";
import CartListHeader from "./CartListHeader";

class CartList extends Component {
    constructor() {
        super();
        this.state = {
            cartList: [
                {
                    price: "56$",
                    count: "1",
                }, {
                    price: "56$",
                    count: "1",
                }, {
                    price: "56$",
                    count: "1",
                }, {
                    price: "56$",
                    count: "1",
                }, {
                    price: "56$",
                    count: "1",
                }, {
                    price: "56$",
                    count: "1",
                }, {
                    price: "56$",
                    count: "1",
                }, {
                    price: "56$",
                    count: "1",
                },
            ]
        }
    }

    render() {
        let cartList = this.state.cartList.map(cartList => {
            return (
                <CartItem cartList={cartList}/>
            )
        });
        return (
            <div>
                <Container className="pb-5 pt-2">
                    {/*Cart List - Header*/}
                    <CartListHeader/>

                    {/*Cart List - Items*/}
                    {cartList}
                </Container>
            </div>
        );
    }
}

export default CartList;