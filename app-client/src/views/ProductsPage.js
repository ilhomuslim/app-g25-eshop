import React from "react";

// Components - reactstrap
import {Col, Container, Row} from "reactstrap";

// Components
import Header from "./components/Header.js";
import Footer from "./components/Footer.js";

// Pages
import NavigationBar from "./components/NavigationBar";
import Sidebar from "./components/Sidebar";
import ProductContent from "./product/ProductContent";

function Shop() {
    return (
        <>
            {/*Headers*/}
            <Header/>
            <NavigationBar/>

            {/*Sidebar -Category/Brands & Content - Product List*/}
            <Container>
                <Row>
                    <Col md="3">
                        <Sidebar/>
                    </Col>
                    <Col md="9">
                        <ProductContent/>
                    </Col>
                </Row>
            </Container>

            {/*Footers*/}
            <Footer/>
        </>
    );
}

export default Shop;
