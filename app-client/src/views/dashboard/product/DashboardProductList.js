import React, {Component, useState} from 'react';

import {
    Button,
    Col,
    Form, Input,
    InputGroup,
    InputGroupAddon, InputGroupText,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    Table, Container
} from "reactstrap";

import ClientModalAddEdit from "./ClientModalAddEdit";
import ClientModalDelete from "./ClientModalDelete";

import {connect} from 'react-redux'
import axios from 'axios'

class DashboardProductList extends Component {

    state={
        modalVisible:false,
    };

    componentDidMount() {
        this.refreshList();
    }

    refreshList = () => {
        axios.get("http://localhost/api/product",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                console.log(data.data._embedded.products);
                this.props.setProductsList(data.data._embedded.products);
            });
        axios.get("http://localhost/api/category",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                // console.log(data.data._embedded.categories);
                this.props.setCategoryList(data.data._embedded.categories);
            });
        axios.get("http://localhost/api/brand",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                // console.log(data.data._embedded.categories);
                this.props.setBrandList(data.data._embedded.brands);
            });
        axios.get("http://localhost/api/tag",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                // console.log(data.data._embedded.categories);
                this.props.setTagList(data.data._embedded.tags);
            });
        axios.get("http://localhost/api/company",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                this.props.setCompanyList(data.data._embedded.companies);
            });
    };

    handleEdit = client => {
        this.setState({
            currentClient:client
        });
        this.openAddModal()
    };

    openAddModal = () => {
        this.setState({
            modalVisible:!this.state.modalVisible
        })
    };

    saveProduct = () => {
        const name = document.getElementById("productName").value;
        const webId = document.getElementById("webId").value;
        const price = document.getElementById("price").value;
        const count = document.getElementById("count").value;
        const condition = document.getElementById("condition").value;
        const category = document.getElementById("categories").value;
        const brand = document.getElementById("brands").value;
        const details = document.getElementById("details").value;
        const tag = document.getElementById("tag").value;
        const company = document.getElementById("company").value;
        axios.post("http://localhost/api/product",
            {"name": name, "category": category, "webId":webId,
            "price":price, "count":count, "condition":condition, "brand": brand,
            "details":details, "tags":[tag], "company":company},
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                this.refreshList();
                this.openAddModal();
            });
    };

   /* editClient = () => {
        const name = document.getElementById("brandName").value;
        const category = document.getElementById("categories").value;
        axios.put("http://localhost/api/user/"+this.state.currentClient.id,
            {"username": username, "email": email, "phone":phone, "balance":balance},
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                this.refreshList();
                this.openAddModal();
            });
    };*/

    render() {
        return (
            <div>
                <Container className="pb-5 pt-2">
                <Row className="border">
                    <Button onClick={this.openAddModal}>Add product</Button>
                    <Col className="text-center" md={12}>
                        <Table>
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Web id</th>
                                <th>Price</th>
                                <th>Count</th>
                                <th>Condition</th>
                                <th>Category</th>
                                <th>Brand</th>
                                <th>Details</th>
                                <th>Tags</th>
                                <th>Company</th>
                                <th>Operations</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.productList.map(product =>{
                                return <tr key={product.id}>
                                    <td>{product.name}</td>
                                    <td>{product.webId}</td>
                                    <td>{product.price}</td>
                                    <td>{product.count}</td>
                                    <td>{product.condition}</td>
                                    <td>{product.categoryName}</td>
                                    <td>{product.brandName}</td>
                                    <td>{product.details}</td>
                                    <td>{product.tags[0].name}</td>
                                    <td>{product.companyName}</td>
                                    <Button className="d-inline-block " onClick={() => this.handleEdit(product)}>Edit</Button>
                                    <ClientModalDelete/>
                                </tr>
                            })}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Modal isOpen={this.state.modalVisible} toggle={this.openAddModal}>
                    <ModalHeader toggle={this.openAddModal}>Product modal</ModalHeader>
                    <ModalBody>
                        <Form className="register-form">
                            <label className="pt-2 text-dark">Name</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="productName" placeholder="name" type="text"/>
                            </InputGroup>
                            <label className="pt-2 text-dark">Web id</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="webId" placeholder="web id" type="number"/>
                            </InputGroup>
                            <label className="pt-2 text-dark">Price</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="price" placeholder="price" type="number"/>
                            </InputGroup>
                            <label className="pt-2 text-dark">Count</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="count" placeholder="count" type="number"/>
                            </InputGroup>
                            <label className="pt-2 text-dark">Condition</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="condition" placeholder="condition" type="text"/>
                            </InputGroup>
                            <label className="pt-2 text-dark">Category</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input type="select" name="category" id="categories">
                                    <option>Select category</option>
                                    {this.props.categoryList.map(category => {
                                        return <option value={'/api/category/' + category.id}>{category.name}</option>
                                    })}
                                </Input>
                            </InputGroup>
                            <label className="pt-2 text-dark">Brand</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input type="select" name="brand" id="brands">
                                    <option>Select brand</option>
                                    {this.props.brandList.map(brand => {
                                        return <option value={'/api/brand/' + brand.id}>{brand.name}</option>
                                    })}
                                </Input>
                            </InputGroup>
                            <label className="pt-2 text-dark">Details</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="details" placeholder="details" type="text"/>
                            </InputGroup>
                            <label className="pt-2 text-dark">Tags</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input type="select" name="tag" id="tag">
                                    <option>Select tag</option>
                                    {this.props.tagList.map(tag => {
                                        return <option value={'/api/tag/' + tag.id}>{tag.name}</option>
                                    })}
                                </Input>
                            </InputGroup>
                            <label className="pt-2 text-dark">Company</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input type="select" name="company" id="company">
                                    <option>Select company</option>
                                    {this.props.companyList.map(company => {
                                        return <option value={'/api/company/' + company.id}>{company.name}</option>
                                    })}
                                </Input>
                            </InputGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.saveProduct}>Save product</Button>{' '}
                        <Button color="secondary" onClick={this.openAddModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    setBrandList: brands => dispatch({type:"brandsList", brandList:brands}),
    setCategoryList: categories => dispatch({type:"categoryList", categoryList:categories}),
    setProductsList: products => dispatch({type:"productList", productList:products}),
    setTagList: tags => dispatch({type:"tagList", tagList:tags}),
    setCompanyList: companies => dispatch({type:"companyList", companyList:companies}),
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardProductList);