import React, {Component, useState} from 'react';

import {
    Button,
    Col,
    Form, Input,
    InputGroup,
    InputGroupAddon, InputGroupText,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    Table, Container
} from "reactstrap";

import ClientModalAddEdit from "./ClientModalAddEdit";
import ClientModalDelete from "./ClientModalDelete";

import {connect} from 'react-redux'
import axios from 'axios'

class DashboardBrandList extends Component {

    state={
        modalVisible:false,
        currentClient:{}
    };

    componentDidMount() {
        this.refreshList();
    }

    refreshList = () => {
        axios.get("http://localhost/api/brand",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                console.log(data.data._embedded.brands);
                this.props.setBrandsList(data.data._embedded.brands);
            });
        axios.get("http://localhost/api/category",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                // console.log(data.data._embedded.categories);
                this.props.setCategoryList(data.data._embedded.categories);
            })
    };

    handleEdit = client => {
        this.setState({
            currentClient:client
        });
        this.openAddModal()
    };

    openAddModal = () => {
        this.setState({
            modalVisible:!this.state.modalVisible
        })
    };

    saveClient = () => {
        const name = document.getElementById("brandName").value;
        const category = document.getElementById("categories").value;
        axios.post("http://localhost/api/brand",
            {"name": name, "categories": [category]},
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                this.refreshList();
                this.openAddModal();
            });
    };

   /* editClient = () => {
        const name = document.getElementById("brandName").value;
        const category = document.getElementById("categories").value;
        axios.put("http://localhost/api/user/"+this.state.currentClient.id,
            {"username": username, "email": email, "phone":phone, "balance":balance},
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                this.refreshList();
                this.openAddModal();
            });
    };*/

    render() {
        return (
            <div>
                <Container className="pb-5 pt-2">
                <Row className="border">
                    <Button onClick={this.openAddModal}>Add brand</Button>
                    <Col className="text-center" md={12}>
                        <Table>
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Categories</th>
                                <th>Operations</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.brandList.map(brand =>{
                                return <tr key={brand.id}>
                                    <td>{brand.name}</td>
                                    <td>{brand.categories[0].name}</td>
                                    <Button className="d-inline-block " onClick={() => this.handleEdit(brand)}>Edit</Button>
                                    <ClientModalDelete/>
                                </tr>
                            })}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Modal isOpen={this.state.modalVisible} toggle={this.openAddModal}>
                    <ModalHeader toggle={this.openAddModal}>Modal title</ModalHeader>
                    <ModalBody>
                        <Form className="register-form">
                            <label className="pt-2 text-light">Name</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="brandName" placeholder="name" type="text"/>
                            </InputGroup>
                            <label className="pt-2 text-light">E-mail</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input type="select" name="category" id="categories">
                                    <option>Select category</option>
                                    {this.props.categoryList.map(category => {
                                        return <option value={'/api/category/' + category.id}>{category.name}</option>
                                    })}
                                </Input>
                            </InputGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.saveClient}>Save brand</Button>{' '}
                        <Button color="secondary" onClick={this.openAddModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    setBrandsList: brands => dispatch({type:"brandsList", brandList:brands}),
    setCategoryList: categories => dispatch({type:"categoryList", categoryList:categories})
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardBrandList);