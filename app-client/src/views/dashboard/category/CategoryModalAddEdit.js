import React, {useState} from "react";

// reactstrap components
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    InputGroup,
    InputGroupAddon,
    InputGroupText, Input, Form,
} from "reactstrap";

function CategoryModalAddEdit() {
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    return (
        <>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Modal title</ModalHeader>
                <ModalBody>
                    <Form className="register-form">
                        <label className="pt-2 text-light">name</label>
                        <InputGroup className="form-group-no-border">
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <i className="nc-icon nc-email-85"/>
                                </InputGroupText>
                            </InputGroupAddon>
                            <Input id="categoryUsername" placeholder="name" type="text"/>
                        </InputGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggle}>Do Something</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </>
    );
}

export default CategoryModalAddEdit;
