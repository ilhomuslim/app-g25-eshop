import React, {Component, useState} from 'react';

import {
    Button,
    Col,
    Form, Input,
    InputGroup,
    InputGroupAddon, InputGroupText,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
} from "reactstrap";

import CategoryModalAddEdit from "./CategoryModalAddEdit";
import CategoryModalDelete from "./CategoryModalDelete";

import {connect} from 'react-redux'
import axios from 'axios/index'

class DashboardCategoryListItem extends Component {

    state={
        modalSee:false,
        currents:{}
    };

    componentDidMount() {
        this.refreshList();
    }

    refreshList = () => {
        console.log(localStorage.getItem("eshop-token"))
        axios.get("http://localhost/api/category",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                console.log(data)
                this.props.setCategoryList(data.data._embedded.categories);
            })
    };

    handleEdit = category => {
        this.setState({
            currentsCategory:category
        });
        this.openAddModal()
    };

    openAddModal = () => {
        this.setState({
            modalSee:!this.state.modalSee
        })
    };
    save = () =>{
        const name =document.getElementById("categoryName").value;
        axios.post("http://localhost/api/category", {"name":name},
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            }
            )
            .then(data =>{
                this.openAddModal();
                this.refreshList();
            })
    };

    editCategory = () => {
        const name = document.getElementById("categoryName").value;

        axios.put("http://localhost/api/category/"+this.state.currentsCategory.id,
            {"username": name},
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                this.refreshList();
                this.openAddModal();
            });
    };

    render() {
        return (
            <div>
                <Row className="border">
                    <Col className="text-center" md={12}>
                        <thead>
                        <tr>
                            <th>Name</th>

                        </tr>
                        </thead>
                        <tbody>
                        {this.props.categoryList.map(category =>{
                            return <tr key={category.id}>
                                <td>{category.name}</td>
                                <Button className="d-inline-block " onClick={() => this.handleEdit(category)}>Edit</Button>
                                <CategoryModalDelete/>
                            </tr>
                        })}
                        </tbody>
                    </Col>
                </Row>
                <Button onClick={this.openAddModal}>Add category</Button>
                <Modal isOpen={this.state.modalSee} toggle={this.openAddModal}>
                    <ModalHeader toggle={this.openAddModal}>Modal title</ModalHeader>
                    <ModalBody>
                        <Form className="register-form">
                            <label className="pt-2 text-light">Name</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="categoryName" defaultValue={this.state.currents.name} placeholder="name" type="text"/>
                            </InputGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.save} >Edit category</Button>{' '}
                        <Button color="secondary" onClick={this.openAddModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    setCategoryList: category => dispatch({type:"categoryList", categoryList:category})
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardCategoryListItem);