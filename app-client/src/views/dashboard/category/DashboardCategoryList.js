import React, {Component} from 'react';

import {Col, Container, Row} from "reactstrap";

import CartItem from "./DashboardCategoryListItem";
import CartListHeader from "./DashboardCategoryListHeader";

class DashboardCategoryList extends Component {

    render() {
        return (
            <div>
                <Container className="pb-5 pt-2">
                    {/*Cart List - Header*/}
                    <CartListHeader/>

                    {/*Cart List - Items*/}
                    <CartItem/>
                </Container>
            </div>
        );
    }
}

export default DashboardCategoryList;