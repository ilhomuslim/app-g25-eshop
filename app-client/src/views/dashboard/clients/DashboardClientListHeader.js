import React, {Component} from 'react';
import {Button, Col, Container, Row, Table} from "reactstrap";

class DashboardClientListHeader extends Component {
    render() {
        return (
            <div>
                <Row className="name-row bg-secondary p-3 text-light">
                    <Col className="text-center" md={2}>Picture</Col>
                    <Col className="text-center" md={2}>Username</Col>
                    <Col className="text-center" md={2}>Password</Col>
                    <Col className="text-center" md={4}>E-mail</Col>
                </Row>
            </div>
        );
    }
}

export default DashboardClientListHeader;