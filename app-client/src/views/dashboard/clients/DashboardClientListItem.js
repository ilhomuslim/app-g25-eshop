import React, {Component, useState} from 'react';

import {
    Button,
    Col,
    Form, Input,
    InputGroup,
    InputGroupAddon, InputGroupText,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    Table
} from "reactstrap";

import ClientModalAddEdit from "./ClientModalAddEdit";
import ClientModalDelete from "./ClientModalDelete";

import {connect} from 'react-redux'
import axios from 'axios'

class DashboardClientListItem extends Component {

    state={
        modalVisible:false,
        currentClient:{}
    };

    componentDidMount() {
        this.refreshList();
    }

    refreshList = () => {
        axios.get("http://localhost/api/user",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(users => {
                console.log(users);
                this.props.setClientsList(users.data._embedded.userses);
            })
    };

    handleEdit = client => {
        this.setState({
            currentClient:client
        });
        this.openAddModal()
    };

    openAddModal = () => {
        this.setState({
            modalVisible:!this.state.modalVisible
        })
    };

    editClient = () => {
        const username = document.getElementById("clientUsername").value;
        const email = document.getElementById("clientEmail").value;
        const phone = document.getElementById("clientPhone").value;
        const balance = document.getElementById("clientBalance").value;
        axios.put("http://localhost/api/user/"+this.state.currentClient.id,
            {"username": username, "email": email, "phone":phone, "balance":balance},
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
                this.refreshList();
                this.openAddModal();
            });
    };

    render() {
        return (
            <div>
                <Row className="border">
                    <Col className="text-center" md={12}>
                        <Table>
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Balance</th>
                                <th>Operations</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.clientList.map(client =>{
                                return <tr key={client.id}>
                                    <td>{client.username}</td>
                                    <td>{client.email}</td>
                                    <td>{client.phone}</td>
                                    <td>{client.balance}</td>
                                    <Button className="d-inline-block " onClick={() => this.handleEdit(client)}>Edit</Button>
                                    <ClientModalDelete/>
                                </tr>
                            })}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Button onClick={this.openAddModal}>Add User</Button>
                <Modal isOpen={this.state.modalVisible} toggle={this.openAddModal}>
                    <ModalHeader toggle={this.openAddModal}>Modal title</ModalHeader>
                    <ModalBody>
                        <Form className="register-form">
                            <label className="pt-2 text-light">Username</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="clientUsername" defaultValue={this.state.currentClient.username} placeholder="Username" type="text"/>
                            </InputGroup>
                            <label className="pt-2 text-light">E-mail</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="clientEmail" defaultValue={this.state.currentClient.email} placeholder="E-mail" type="text"/>
                            </InputGroup>
                            <label className="pt-2 text-light">Phone</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="clientPhone" defaultValue={this.state.currentClient.phone} placeholder="Phone" type="text"/>
                            </InputGroup>
                            <label className="pt-2 text-light">Balance</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="clientBalance" defaultValue={this.state.currentClient.balance} placeholder="Balance" type="text"/>
                            </InputGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.editClient}>Edit client</Button>{' '}
                        <Button color="secondary" onClick={this.openAddModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    setClientsList: clients => dispatch({type:"clientsList", clientList:clients})
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardClientListItem);