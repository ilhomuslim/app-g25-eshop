import React, {Component} from 'react';

import {Col, Container, Row} from "reactstrap";

import NavigationBar from "../components/NavigationBarDashboard";
import Sidebar from "./SidebarDashboard";
import Footer from "../components/Footer";


import DashboardList from "./clients/DashboardClientList";

class DashboardClientPage extends Component {
    render() {
        return (
            <div>
                {/*Headers*/}
                <NavigationBar/>

                {/*Sidebar -Categories & Content - Product List*/}
                <Container className="pb-lg-5">
                    <Row>
                        <Col className="p-2" md="2">
                            <Sidebar/>
                        </Col>
                        <Col className="p-2">
                            <DashboardList/>
                        </Col>
                    </Row>
                </Container>

                {/*Footers*/}
                <Footer/>
            </div>
        );
    }
}

export default DashboardClientPage;