import React, {Component} from 'react';

import {Button, Col, Container, Form, FormGroup, Input, Label, Row, UncontrolledTooltip} from "reactstrap";

import ProductDetailsCarousel from "./ProductDetailsCarousel";

class ProductInfo extends Component {
    render() {
        return (
            <div>
                <Row>
                    <Col md={4} className="mx-4">
                        <img className="border border-danger"/>
                    </Col>
                    <Col className="border border-danger">
                        <div className="p_info pl-5 pt-5 pb-5">
                            <h5>Anne Klein Sleeveless Colorblock Scuba</h5>
                            <p className="text-muted">Web ID: 1089772</p>
                            <img className="d-block mb-3 text_price"/>
                            <Form inline>
                                <span className="font-weight-bold textPrice">US $59</span>
                                <Label
                                    className="font-weight-bold text-muted d-inline-block mr-2 ml-5">Quantity:</Label>
                                <Input type="text" name="quantity" id="quantity"
                                       className="quantity_input rounded-0 mx-3"/>
                                <Button type="button"
                                        className="font-weight-bold warningBtn border-0 rounded-0  d-inline-block">
                                    Add to cart
                                </Button>
                            </Form>
                            <p className="m-0 mt-2 text-muted "><b
                                className="text-muted ">Availability: </b>In Stock</p>
                            <p className="m-0 text-muted"><b className="text-muted ">Condition: </b>New</p>
                            <p className="m-0 text-muted"><b className="text-muted ">Brand: </b> E-Shoper
                            </p>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col md={4} className="mx-4 rounded-0 pr-0 my-4">
                        <ProductDetailsCarousel/>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default ProductInfo;