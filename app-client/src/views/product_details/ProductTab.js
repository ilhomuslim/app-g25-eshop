import React, {useState} from "react";

// Components - reactstrap
import {NavItem, NavLink, Nav, TabContent, TabPane,} from "reactstrap";

// Components
import ProductCards from "../components/ProductCards";
import ProductTabDetails from "./ProductTabDetails";
import ProductTags from "./ProductTabTags";
import CompanyProfile from "./ProductTabCompanyProfile";
import ProductTabReviews from "./ProductTabReviews";

function ShopContent() {
    const [activeTab, setActiveTab] = React.useState("1");
    const toggle = tab => {
        if (activeTab !== tab) {
            setActiveTab(tab);
        }
    };

    return (
        <div>
            <div className="nav-tabs-navigation">
                <div className="nav-tabs-wrapper">
                    <Nav id="tabs" role="tablist" tabs>
                        <NavItem>
                            <NavLink
                                className={activeTab === "1" ? "active" : ""}
                                onClick={() => {
                                    toggle("1");
                                }}>
                                DETAILS
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={activeTab === "2" ? "active" : ""}
                                onClick={() => {
                                    toggle("2");
                                }}>
                                COMPANY PROFILE
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={activeTab === "3" ? "active" : ""}
                                onClick={() => {
                                    toggle("3");
                                }}>
                                TAG
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={activeTab === "4" ? "active" : ""}
                                onClick={() => {
                                    toggle("4");
                                }}>
                                REVIEWS
                            </NavLink>
                        </NavItem>
                    </Nav>
                </div>
            </div>
            <TabContent activeTab={activeTab} className="text-center">
                <TabPane tabId="1">
                    <ProductTabDetails/>
                </TabPane>
                <TabPane tabId="2">
                    <CompanyProfile/>
                </TabPane>
                <TabPane tabId="3">
                    <ProductTags/>
                </TabPane>
                <TabPane tabId="4">
                    <ProductTabReviews/>
                </TabPane>
            </TabContent>
        </div>
    );
}

export default ShopContent;
