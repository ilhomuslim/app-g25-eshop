import React, {useState} from "react";

// Components
import ProductCards from "../components/ProductCards";

function ProductContent() {
    return (
        <div>
            {/*Products*/}
            <h3 className="mr-2 text-center p-2">Products</h3>
            <ProductCards/>
        </div>
    );
}

export default ProductContent;
