import React, {useState} from "react";

// Components - reactstrap
import {NavItem, NavLink, Nav, TabContent, TabPane,} from "reactstrap";

// Components
import ProductCards from "../components/ProductCards";

function ShopContent() {
    const [activeTab, setActiveTab] = React.useState("1");
    const toggle = tab => {
        if (activeTab !== tab) {
            setActiveTab(tab);
        }
    };

    return (
        <div>
            <div className="nav-tabs-navigation p-4">
                <div className="nav-tabs-wrapper">
                    <Nav id="tabs" role="tablist" tabs>
                        <NavItem>
                            <NavLink
                                className={activeTab === "1" ? "active" : ""}
                                onClick={() => {
                                    toggle("1");
                                }}>
                                T-SHIRT
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={activeTab === "2" ? "active" : ""}
                                onClick={() => {
                                    toggle("2");
                                }}>
                                BLAZERS
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={activeTab === "3" ? "active" : ""}
                                onClick={() => {
                                    toggle("3");
                                }}>
                                SUNGLASS
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={activeTab === "4" ? "active" : ""}
                                onClick={() => {
                                    toggle("4");
                                }}>
                                KIDS
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={activeTab === "5" ? "active" : ""}
                                onClick={() => {
                                    toggle("5");
                                }}>
                                POLO SHIRT
                            </NavLink>
                        </NavItem>
                    </Nav>
                </div>
            </div>
            <TabContent activeTab={activeTab} className="text-center">
                <TabPane tabId="1">
                    <ProductCards/>
                </TabPane>
                <TabPane tabId="2">
                    <ProductCards/>
                </TabPane>
                <TabPane tabId="3">
                    <ProductCards/>
                </TabPane>
                <TabPane tabId="4">
                    <ProductCards/>
                </TabPane>
                <TabPane tabId="5">
                    <ProductCards/>
                </TabPane>
            </TabContent>
        </div>
    );
}

export default ShopContent;
