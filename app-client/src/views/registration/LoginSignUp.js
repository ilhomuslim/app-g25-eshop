import React, {Component} from 'react';

// reactstrap components
import {
    Button,
    Card,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Container,
    Row,
    Col
} from "reactstrap";

import axios from 'axios';

class LoginSignUp extends Component {
    handleSignIn = () => {
        const username = document.getElementById("signInUsername").value;
        const password = document.getElementById("signInPass").value;
        axios.post("http://localhost/api/sign/in",
            {"userName": username, "password": password})
            .then(data => {
                localStorage.setItem("eshop-token", data.data.accessToken);
            });
        console.log(localStorage.length);
    };
    handleSignUp = () => {
        const username = document.getElementById("signUpUsername").value;
        const password = document.getElementById("signUpPass").value;
        const email = document.getElementById("signUpEmail").value;
        axios.post("http://localhost/api/sign/up",
            {"userName": username, "password": password, "email":email})
            .then(data => {
                localStorage.setItem("eshop-token", data.data.accessToken);
            });
        console.log(localStorage.length);
    };

    render() {
        return (
            <div className="mb-lg-5 pt-lg-5 pb-lg-5">
                <Container className="mb-5">
                    <Row>
                        <Col className="mx-auto" lg="4" md="6">
                            <Card className="p-3 m-2 card-register d-inline-block bg-dark">
                                <h3 className="title mx-auto text-xl-center text-light">Login to your account</h3>
                                <Form className="register-form">
                                    <label className="pt-2 text-light">Username</label>
                                    <InputGroup className="form-group-no-border">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="nc-icon nc-email-85"/>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input id="signInUsername" placeholder="Username" type="text"/>
                                    </InputGroup>
                                    <label className="pt-2 text-light">Password</label>
                                    <InputGroup className="form-group-no-border">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="nc-icon nc-key-25"/>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input id="signInPass" placeholder="Password" type="password"/>
                                    </InputGroup>
                                    <Button onClick={this.handleSignIn} block className="mt-4 btn-round" color="danger"
                                            type="button">
                                        Login
                                    </Button>
                                </Form>
                                <div className="forgot text-center">
                                    <Button className="mt-2 btn-link text-secondary" color="dark">
                                        Forgot password?
                                    </Button>
                                </div>
                            </Card>
                        </Col>
                        <Col className="mx-auto" lg="4" md="6">
                            <Card className="p-3 m-2 card-register d-inline-block bg-dark">
                                <h3 className="title mx-auto text-xl-center text-light">New User SignUp!</h3>
                                <Form className="register-form">
                                    <label className="pt-2 text-light">Username</label>
                                    <InputGroup className="form-group-no-border">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="nc-icon nc-email-85"/>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input id="signUpUsername" placeholder="Username" type="text"/>
                                    </InputGroup>
                                    <label className="pt-2 text-light">E-mail</label>
                                    <InputGroup className="form-group-no-border">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="nc-icon nc-email-85"/>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input id="signUpEmail" placeholder="E-mail" type="text"/>
                                    </InputGroup>
                                    <label className="pt-2 text-light">Password</label>
                                    <InputGroup className="form-group-no-border">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="nc-icon nc-key-25"/>
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input id="signUpPass" placeholder="Password" type="password"/>
                                    </InputGroup>
                                    <Button onClick={this.handleSignUp} block className="mt-4 btn-round" color="danger"
                                            type="button">
                                        SignUp
                                    </Button>
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default LoginSignUp;
