import React from "react";

// Components
import Footer from "./components/Footer";
import NavigationBar from "./components/NavigationBar";
import Header from "./components/Header";

//Pages
import LoginSignUp from "./registration/LoginSignUp";

function Shop() {
    return (
        <>
            {/*Headers*/}
            <Header/>
            <NavigationBar/>

            {/*Login / SignUp Form*/}
            <LoginSignUp/>

            {/*Footers*/}
            <Footer/>
        </>
    );
}

export default Shop;
