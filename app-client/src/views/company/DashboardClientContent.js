import React, {Component} from 'react';

// Components
import Header from "../components/Header.js";
import Footer from "../components/Footer.js";

// Pages
import CartList from "./clients/DashboardClientList";
import NavigationBar from "../components/NavigationBar";

class DashboardClientContent extends Component {
    render() {
        return (
            <>
                {/*Headers*/}
                <Header/>
                <NavigationBar/>

                {/*Cart List*/}
                <CartList/>

                {/*Footers*/}
                <Footer/>
            </>
        );
    }
}

export default DashboardClientContent;