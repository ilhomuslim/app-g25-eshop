import React, {useState} from "react";

// Components
import ProductCards from "../components/ProductCards";

function ShopContent() {
    return (
        <div>
            {/*Products*/}
            <h3 className="mr-2 text-center p-2">Products</h3>
            <ProductCards/>

            {/*Categories Tabs*/}
            {/*<ShopContentTab/>*/}

            {/*Recommended*/}
            <h3 className="mr-2 text-center p-2">Recommended</h3>
            <ProductCards/>
        </div>
    );
}

export default ShopContent;
