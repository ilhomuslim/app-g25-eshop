import React, {useState} from "react";

// reactstrap components
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, Col,} from "reactstrap";

function ClientModal() {
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    return (
        <>

            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Delete Company</ModalHeader>
                <ModalBody>
                    <h3>Rostdan o'chirmoqchimisiz</h3>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggle}>Yes</Button>
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </>
    );
    }


export default ClientModal;
