import React, {useState} from "react";
import {connect} from "react-redux";

// reactstrap components
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Col,
    InputGroup,
    InputGroupAddon,
    InputGroupText, Input, Form,
} from "reactstrap";


function ClientModalAddEdit() {
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    return (
        <>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Edit Company</ModalHeader>
                <ModalBody>
                    <Form className="register-form">
                        <label className="pt-2 text-light">Username</label>
                        <InputGroup className="form-group-no-border">
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <i className="nc-icon nc-email-85"/>
                                </InputGroupText>
                            </InputGroupAddon>
                            <Input id="clientUsername" placeholder="Username" type="text"/>
                        </InputGroup>
                        <label className="pt-2 text-light">E-mail</label>
                        <InputGroup className="form-group-no-border">
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <i className="nc-icon nc-email-85"/>
                                </InputGroupText>
                            </InputGroupAddon>
                            <Input id="clientEmail" placeholder="E-mail" type="text"/>
                        </InputGroup>
                        <label className="pt-2 text-light">Phone</label>
                        <InputGroup className="form-group-no-border">
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <i className="nc-icon nc-email-85"/>
                                </InputGroupText>
                            </InputGroupAddon>
                            <Input id="clientPhone" placeholder="Phone" type="text"/>
                        </InputGroup>
                        <label className="pt-2 text-light">Balance</label>
                        <InputGroup className="form-group-no-border">
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                    <i className="nc-icon nc-email-85"/>
                                </InputGroupText>
                            </InputGroupAddon>
                            <Input id="clientBalance" defaultValue="" placeholder="Balance" type="text"/>
                        </InputGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggle}>Do Something</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </>
    );
}
/*const mapStateToProps = state =>({
    ...state
});
 const mapDispatchToProps = dispatch =>({
    setNewCompany: company => dispatch({type:companyList, companyList:company})
 });*/


/*export default connect(mapStateToProps,mapDispatchToProps)(ClientModalAddEdit);*/
export  default ClientModalAddEdit;