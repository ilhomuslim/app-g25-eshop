import React, {Component, useState} from 'react';
import {
    Button,
    Col,
    Form, Input,
    InputGroup,
    InputGroupAddon, InputGroupText,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    Table
} from "reactstrap";

import ClientModalAddEdit from "./ClientModalAddEdit";
import ClientModalDelete from "./ClientModalDelete";

import {connect} from 'react-redux'
import axios from 'axios'

class DashboardCompanyListItem extends Component {

    state={
        modalVisible:false,
        currentCompany:{},
        edit:true
    };

    componentDidMount() {
        this.refreshList();
    }



    refreshList = () => {
        axios.get("http://localhost/api/company",
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(company => {
             console.log(company);
                this.props.setCompanyList(company.data._embedded.companies);
            })
    };


    handleDelete = () =>{
        axios.delete("http://localhost/api/company/"+this.state.currentCompany.id,
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")
                }
            })
            .then(data => {
            });
        this.refreshList();
        alert("O'chirildi!");
    };


saveCompany = () =>{
    const name = document.getElementById("companyName").value;
    const phoneNumber = document.getElementById("companyPhone").value;
    const webSite = document.getElementById("companyWebSite").value;
    const address = document.getElementById("companyAddress").value;
    axios.post("http://localhost/api/company",
        {"name": name, "phoneNumber": phoneNumber, "webSite":webSite, "address":address},
        {
            headers:{ "Authorization":"Bearer " + localStorage.getItem("eshop-token")}
        }
    ).then(newcompany =>{
        this.setState({
            companyList:newcompany,
            edit:false
        });

    });
    this.refreshList();
    this.openAddModal();
};

    handleAdd = () => {
        this.setState({
            edit:false
        });
        this.refreshList();
         this.openAddModal();

    };

    handleEdit = company => {
        this.setState({
            currentCompany:company,
            edit:true
        });
        this.openAddModal()
    };

    openAddModal = () => {
        this.setState({
            modalVisible:!this.state.modalVisible
        })
    };

    editCompany = () => {
        const name = document.getElementById("companyName").value;
        const phoneNumber = document.getElementById("companyPhone").value;
        const webSite = document.getElementById("companyWebSite").value;
        const address = document.getElementById("companyAddress").value;
        axios.put("http://localhost/api/company/"+this.state.currentCompany.id,
            {"name": name, "phoneNumber": phoneNumber, "webSite":webSite, "address":address},
            {
                headers:{
                    "Authorization":"Bearer " + localStorage.getItem("eshop-token")}
            })
            .then(data => {
                this.refreshList();
                this.openAddModal();
            });
    };

    render() {
        return (
            <div>
                <Row className="border">
                    <Col className="text-center" md={12}>
                        <Table>
                            <thead>
                            <tr>
                                <th>Company name</th>
                                <th>Phone number</th>
                                <th>Web site</th>
                                <th>Address</th>
                                <th>Operation</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.companyList.map(company =>{
                                return <tr key={company.id}>
                                    <td>{company.name}</td>
                                    <td>{company.phoneNumber}</td>
                                    <td>{company.webSite}</td>
                                    <td>{company.address}</td>
                                    <Button className="d-inline-block " onClick={() => this.handleEdit(company)}>Edit</Button>
                                    <Button className="bg-light text-dark" onClick={()=>this.handleDelete(company)}>Delete</Button>
                                </tr>
                            })}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Button onClick={()=>{
                    this.handleAdd();
                }}>Add Сompany</Button>
                <Modal isOpen={this.state.modalVisible} toggle={this.openAddModal}>
                    <ModalHeader toggle={this.openAddModal}>{this.state.edit?"Edit Company":"Add Company"}</ModalHeader>
                    <ModalBody>
                        <Form className="register-form" >

                            <label className="pt-2 text-light">Company name</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="companyName"
                                       defaultValue={this.state.edit?this.state.currentCompany.name:""}
                                       placeholder="Company name" type="text"/>
                            </InputGroup>


                            <label className="pt-2 text-light">Phone number</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-phone-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="companyPhone"
                                       defaultValue={this.state.edit?this.state.currentCompany.phoneNumber:""}
                                       placeholder="phoneNumber" type="text"/>
                            </InputGroup>
                            <label className="pt-2 text-light">Web site</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="companyWebSite"
                                       defaultValue={this.state.edit?this.state.currentCompany.webSite:""}
                                       placeholder="webSite" type="text"/>
                            </InputGroup>

                            <label className="pt-2 text-light">Address</label>
                            <InputGroup className="form-group-no-border">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        <i className="nc-icon nc-email-85"/>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input id="companyAddress"
                                       defaultValue={this.state.edit?this.state.currentCompany.address:""}
                                       placeholder="Adress" type="text"/>
                            </InputGroup>

                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.state.edit?this.editCompany:this.saveCompany}>{this.state.edit?"Edit client":"Save"}</Button>
                        <Button color="secondary" onClick={this.openAddModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({
    setCompanyList: company => dispatch({type:"companyList", companyList:company})
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardCompanyListItem);