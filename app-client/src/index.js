import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Route, Redirect, Switch} from "react-router-dom";

// styles
import 'bootstrap/dist/css/bootstrap.min.css';

// pages
import Tag from './views/Tag/DashboardTagPage';
import Category from './views/dashboard/category/DashboardCategoryList';
import Company from './views/company/DashboardCompanyPage'
import Registration from "./views/RegistrationPage";
import Profile from "./views/ProfilePage";
import Products from "./views/ProductsPage";
import DashboardProduct from './views/dashboard/DashboardProductPage'
import ProfileEdit from "./views/ProfileEditPage";
import ProductDetails from "./views/ProductDetailsPage";
import CartPage from "./views/CartPage";
import Brand from './views/dashboard/brand/DashboardBrandPage';
import Dashboard from "./views/DashboardPage";
import DashboardClient from "./views/dashboard/DashboardClientPage";
import EShopper from "./views/ShopPage";
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import Reducer from './redux/Reducer'

ReactDOM.render(
    <Provider store={createStore(Reducer,
        {
            clientList:[],
            brandList:[],
            tagList:[],
            companyList:[],
            categoryList:[],
            companyTag:[],
            productList:[]
        })}>
        <BrowserRouter>
            <Switch>
                <Route path="/dashboard" render={props => <Dashboard {...props} />}/>
                <Route path="/dashboard_client" render={props => <DashboardClient {...props} />}/>
                <Route path="/dashboard_category" render={props => <Category {...props} />}/>
                <Route path="/dashboard_brand" render={props => <Brand {...props} />}/>
                <Route path="/dashboard_product" render={props => <DashboardProduct {...props} />}/>
                <Route path="/dashboard_company" render={props => <Company {...props} />}/>
                <Route path="/dashboard_tag" render={props => <Tag {...props} />}/>
                <Route path="/dashboard_login" render={props => <Dashboard {...props} />}/>
                <Route path="/dashboard_profile" render={props => <Dashboard {...props} />}/>
                <Route path="/tag" render={props => <Tag {...props} />}/>
                <Route path="/product" render={props => <Products {...props} />}/>
                <Route path="/product_details" render={props => <ProductDetails {...props} />}/>
                <Route path="/profile" render={props => <Profile {...props} />}/>
                <Route path="/profile_edit" render={props => <ProfileEdit {...props} />}/>
                <Route path="/login" render={props => <Registration {...props} />}/>
                <Route path="/e_shopper" render={props => <EShopper {...props} />}/>
                <Redirect to="/e_shopper"/>
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById("root")
);